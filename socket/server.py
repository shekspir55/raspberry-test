# !/usr/bin/env python
# -*- coding: utf8 -*-
# #
# import RPi.GPIO as GPIO
# import MFRC522
# import signal

import socketio
import eventlet
import eventlet.wsgi
from flask import Flask, render_template

import thread

sio = socketio.Server()
app = Flask(__name__)


@app.route('/')
def index():
    """Serve the client-side application."""
    return render_template('index.html')


@sio.on('connect')
def connect(sid, environ):
    print("connect ", sid)
    sio.emit('rfid', 'Hi Bro')


@sio.on('chat message')
def message(sid, data):
    print("message ", data)
    sio.emit('reply', room=sid)


@sio.on('disconnect')
def disconnect(sid):
    print('disconnect ', sid)


def bg_emit(uid):
    print "Card read UID: " + str(uid[0]) + "," + str(uid[1]) + "," + str(uid[2]) + "," + str(uid[3])
    sio.emit('rfid', str(uid[0]) + "," + str(uid[1]) + "," + str(uid[2]) + "," + str(uid[3]))


# def watchRFID():
#     print 'Watching RFID'
#     continue_reading = True
#
#     # Capture SIGINT for cleanup when the script is aborted
#     def end_read(signal, frame):
#         global continue_reading
#         print "Ctrl+C captured, ending read."
#         continue_reading = False
#         GPIO.cleanup()
#
#     # Hook the SIGINT
#     signal.signal(signal.SIGINT, end_read)
#
#     # Create an object of the class MFRC522
#     MIFAREReader = MFRC522.MFRC522()
#
#     # This loop keeps checking for chips. If one is near it will get the UID and authenticate
#     while continue_reading:
#
#         # Scan for cards
#         (status, TagType) = MIFAREReader.MFRC522_Request(MIFAREReader.PICC_REQIDL)
#
#         # If a card is found
#         if status == MIFAREReader.MI_OK:
#             print "Card detected"
#
#         # Get the UID of the card
#         (status, uid) = MIFAREReader.MFRC522_Anticoll()
#
#         # If we have the UID, continue
#         if status == MIFAREReader.MI_OK:
#             # Print UID
#             bg_emit(uid)


if __name__ == '__main__':
    # wrap Flask application with engineio's middleware
    app = socketio.Middleware(sio, app)

    # eventlet.spawn(watchRFID)
    print 'Running server'
    # deploy as an eventlet WSGI server
    eventlet.wsgi.server(eventlet.listen(('', 8000)), app)
