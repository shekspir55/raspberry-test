var zerorpc = require("zerorpc");

var server = new zerorpc.Server({
    RFID: function(name, reply) {
        console.log(name);
        reply(null, "Hello, " + name, false);
    }
});

server.bind("tcp://0.0.0.0:4242");