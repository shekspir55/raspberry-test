var Gpio = require('onoff').Gpio;
var time = 500; // tree seconds
var port = 1100;
var Promise = require('bluebird');
const express = require('express')
const app = express();

app.get('/startMe', function (req, res) {
    buttonOnnOff(18)
    .then(() => {
        setTimeout(()=>{
        buttonOnnOff(18)    
        }, time);
    })
    res.send('Hello World!');
});


app.get('/locktMe', function (req, res) {
    buttonOnnOff(17)
    res.send('Hello World!');
});

app.get('/stopMe', function (req, res) {
    buttonOnnOff(27)
    res.send('Hello World!');
});

app.listen(port, function () {
  console.log('Example app listening on port ' + port);
});




function buttonOnnOff(number) {
    return new Promise((resolve)=>{
                    led = new Gpio(number, 'out');
                    setTimeout(function () {
                        led.writeSync(0);  // Turn LED off.
                        led.unexport();
                        resolve()
                    },time);
    });
}












